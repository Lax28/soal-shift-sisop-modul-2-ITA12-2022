# soal-shift-sisop-modul-2-ITA12-2022

## Anggota Kelompok
1. Muhammad Jovan Adiwijaya Yanuarsyah (5027201025)
2. Made Gede Krisna Wangsa (5027201047)
3. Nida'ul Faizah (5027201064)

## Description
Laporan ini dibuat dengan tujuan untuk menjelaskan pengerjaan serta permasalahan yang kami alami dalam pengerjaan soal shift sistem operasi modul 2 tahun 2022.

## Soal Shift 1 (revisi)
Mas Refadi adalah seorang wibu gemink. Dan jelas game favoritnya adalah bengshin impek. Terlebih pada game tersebut ada sistem gacha item yang membuat orang-orang selalu ketagihan untuk terus melakukan nya. Tidak terkecuali dengan mas Refadi sendiri. Karena rasa penasaran bagaimana sistem gacha bekerja, maka dia ingin membuat sebuah program untuk men-simulasi sistem history gacha item pada game tersebut. Tetapi karena dia lebih suka nge-wibu dibanding ngoding, maka dia meminta bantuanmu untuk membuatkan programnya. Sebagai seorang programmer handal, bantulah mas Refadi untuk memenuhi keinginan nya itu.

- Saat program pertama kali berjalan. Program akan mendownload file characters dan
file weapons dari link yang ada dibawah, lalu program akan mengekstrak kedua file
tersebut. File tersebut akan digunakan sebagai database untuk melakukan gacha
item characters dan weapons. Kemudian akan dibuat sebuah folder dengan nama
“gacha_gacha” sebagai working directory. Seluruh hasil gacha akan berada di dalam
folder tersebut. Penjelasan sistem gacha ada di poin (d).
- Mas Refadi ingin agar setiap kali gacha, item characters dan item weapon akan selalu
bergantian diambil datanya dari database. Maka untuk setiap kali jumlah-gacha nya
bernilai genap akan dilakukan gacha item weapons, jika bernilai ganjil maka item
characters. Lalu untuk setiap kali jumlah-gacha nya mod 10, maka akan dibuat
sebuah file baru (.txt) dan output hasil gacha selanjutnya akan berada di dalam file
baru tersebut. Dan setiap kali jumlah-gacha nya mod 90, maka akan dibuat sebuah
folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut.
Sehingga untuk setiap folder, akan terdapat 9 file (.txt) yang didalamnya berisi 10
hasil gacha. Dan karena ini simulasi gacha, maka hasil gacha di dalam file .txt adalah
ACAK/RANDOM dan setiap file (.txt) isi nya akan BERBEDA
- Format penamaan setiap file (.txt) nya adalah {Hh:Mm:Ss}_gacha_{jumlah-gacha},
misal 04:44:12_gacha_120.txt, dan format penamaan untuk setiap folder nya adalah
total_gacha_{jumlah-gacha}, misal total_gacha_270. Dan untuk setiap file (.txt) akan
memiliki perbedaan penamaan waktu output sebesar 1 second.
- Pada game tersebut, untuk melakukan gacha item kita harus menggunakan alat tukar
yang dinamakan primogems. Satu kali gacha item akan menghabiskan primogems
sebanyak 160 primogems. Karena mas Refadi ingin agar hasil simulasi gacha nya
terlihat banyak, maka pada program, primogems di awal di-define sebanyak 79000
primogems. Setiap kali gacha, ada 2 properties yang akan diambil dari database,
yaitu name dan rarity. Lalu Outpukan hasil gacha nya ke dalam file (.txt) dengan
format hasil gacha {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}.
Program akan selalu melakukan gacha hingga primogems habis.
Contoh : 157_characters_5_Albedo_53880
- Proses untuk melakukan gacha item akan dimulai bertepatan dengan anniversary
pertama kali mas Refadi bermain bengshin impek, yaitu pada 30 Maret jam 04:44.
Kemudian agar hasil gacha nya tidak dilihat oleh teman kos nya, maka 3 jam setelah
anniversary tersebut semua isi di folder gacha_gacha akan di zip dengan nama
not_safe_for_wibu dengan dipassword "satuduatiga", lalu semua folder akan di
delete sehingga hanya menyisakan file (.zip)

```
void downloadFile(char *down_arg[])
{
  pid_t id = fork();
  int status;
  if (id == 0)
  {
    execv("/bin/wget", down_arg);
  }
  else
  {
    while ((wait(&status)) > 0)
      ;
    printf("download file success\n");
  }
}

void unzipFile(char *unzip_arg[])
{
  pid_t id = fork();
  int status;
  if (id == 0)
  {
    execv("/bin/unzip", unzip_arg);
  }
  else
  {
    while ((wait(&status)) > 0)
      ;
    printf("unzip file success\n");
  }
}

void rmFile(char *rm_arg[])
{
  pid_t id = fork();
  int status;
  if (id == 0)
  {
    execv("/bin/rm", rm_arg);
  }
  else
  {
    while ((wait(&status)) > 0)
      ;
    printf("remove file success\n");
  }
}

void mkdirFile(char *mkdir_arg[])
{
  pid_t id = fork();
  int status;
  if (id == 0)
  {
    execv("/bin/mkdir", mkdir_arg);
  }
  else
  {
    while ((wait(&status)) > 0)
      ;
    printf("mkdir file success\n");
  }
}

void touchFile(char *touch_arg[])
{
  pid_t id = fork();
  int status;
  if (id == 0)
  {
    execv("/bin/touch", touch_arg);
  }
  else
  {
    while ((wait(&status)) > 0)
      ;
    printf("touch file success\n");
  }
}
```

### Beberapa fungsi di atas digunakan untuk mendownload file, unzip file, remove file, membuat direktori baru, dan membuat file baru.

```
void createNewDir()
{
  char *num;
  int gacha = 1;
  int progems = 79000;
  for (int i = progems; i > 0; i -= 160)
  {
    progems -= 160;
    if (progems > 0)
    {
      if (gacha % 90 == 0)
      {
        char dirName[100];
        if (asprintf(&num, "%d", gacha) == -1)
        {
          perror("asprintf");
        }
        strcat(strcpy(dirName, "gacha_gacha/total_gacha_"), num);
        free(num);

        char *args[] = {"mkdir", dirName, NULL};
        mkdirFile(args);
      }
      gacha++;
    }
  }
}
```
### fungsi createNewDir digunakan untuk membuat beberapa folder yang fungsinya untuk menampung beberapa file .txt seperti yang tercantum dalam soal. Setiap gacha % 90 bernilai 0, direktori akan dibuat.

```
void createNewTxt()
{
  char *num;
  int gacha = 1;
  int gacha_num[] = {90, 180, 270, 360, 450};
  int count = 0;
  int progems = 79000;
  for (int i = 79000; i > 0; i -= 160)
  {
    progems -= 160;
    if (progems > 0)
    {
      if (gacha % 10 == 0 && count < 5)
      {
        char fileName[100];
        if (asprintf(&num, "%d", gacha_num[count]) == -1)
        {
          perror("asprintf");
        }
        strcat(strcpy(fileName, "gacha_gacha/total_gacha_"), num);
        strcat(fileName, "/");
        free(num);

        if (asprintf(&num, "%d", gacha) == -1)
        {
          perror("asprintf");
        }
        strcat(fileName, "_gacha_");
        strcat(fileName, num);
        strcat(fileName, ".txt");
        free(num);

        char *args[] = {"touch", fileName, NULL};
        touchFile(args);
      }

      if (gacha % 90 == 0 && gacha != 0)
        count++;
      gacha++;
    }
  }
}
```
### fungsi createNewTxt digunakan untuk membuat file txt baru di dalam folder yang telah dibuat oleh fungsi createNewDir(). setiap jumlah gacha % 10 bernilai 0, file txt akan dibuat dan ada sekitar 9 file di setiap foldernya.

```
int randNum(int min, int max)
{
  return rand() % (max - min + 1) + min;
}

void getNewText()
{
  int progems = 79000;
  int gacha = 1;
  int gachaFile = 10;
  int gacha_num[] = {90, 180, 270, 360, 450};
  int count = 0;
  int num;
  for (int i = progems; i > 0; i -= 160)
  {
    progems -= 160;
    if (progems > 0)
    {
      if (gacha % 2 != 0)
      {
        num = randNum(0, 47);
        jsonParse(gacha, progems, "characters", listChars[num], gacha_num[count], gachaFile);
      }
      else
      {
        num = randNum(0, 129);
        jsonParse(gacha, progems, "weapons", listWeapons[num], gacha_num[count], gachaFile);
      }
      if (gacha % 90 == 0 && gacha != 0 && count < 5)
        count++;
      if (gacha % 10 == 0 && gacha != 0)
        gachaFile += 10;
      gacha++;
    }
  }
}
```
#### fungsi randNum() digunakan untuk mendapatkan nilai random dari rentang angka tertentu. Fungsi tersebut digunakan pada fungsi getNewText(). Fungsi ini adalah fungsi utama yang nantinya akan memanggil fungsi - fungsi lain. Fungsi ini berguna untuk menentukan file apa yang akan diparsing dahulu. Jika jumlah gacha bernilai genap, maka akan diparsing file characters. Sedangkan untuk gacha bernilai ganjil akan diparsing file weapon. kemudian untuk parameter ketiga pada jsonParse digunakan untuk mengambil list random dari array listChars atau listWeapons yang berisi masing - masing file json di dalam folder characters dan weapons. Untuk parameter kelima berguna untuk menentukan direktori untuk meletakkan hasil json yang telah diparsing, begitu juga dengan gachaFile.

```
void jsonParse(int gacha, int sisa, char directory[], char jsonFile[], int gachaDir, int gachaFile)
{
  FILE *fp;
  char buffer[10000];
  struct json_object *parsed_json;
  struct json_object *name;
  struct json_object *rarity;

  // create fileName
  char fileName[1024];
  strcat(strcpy(fileName, ""), directory);
  strcat(fileName, "/");
  strcat(fileName, jsonFile);

  fp = fopen(fileName, "r");
  fread(buffer, 10000, 1, fp);
  fclose(fp);

  parsed_json = json_tokener_parse(buffer);

  json_object_object_get_ex(parsed_json, "name", &name);
  json_object_object_get_ex(parsed_json, "rarity", &rarity);

  createNewText(gacha, directory, rarity, name, sisa, gachaDir, gachaFile);
}
```
### fungsi jsonParse digunakan untuk memparsing tiap - tiap file Json sesuai dengan parameter yang diberikan. Hasilnya akan dimasukkan ke fungsi CreateNewText untuk dijadikan sebuah kalimat.

```
void createNewText(int gacha, char type[], struct json_object *rarity, struct json_object *name, int sisa, int gachaDir, int gachaFile)
{
  char textName[1024];
  char *num;

  if (asprintf(&num, "%d", gacha) == -1)
  {
    perror("asprintf");
  }
  strcat(strcpy(textName, ""), num);
  strcat(textName, "_");
  strcat(textName, type);
  strcat(textName, "_");
  free(num);

  if (asprintf(&num, "%d", json_object_get_int(rarity)) == -1)
  {
    perror("asprintf");
  }

  strcat(textName, num);
  strcat(textName, "_");
  strcat(textName, json_object_get_string(name));
  strcat(textName, "_");
  free(num);
  if (asprintf(&num, "%d", sisa) == -1)
  {
    perror("asprintf");
  }
  strcat(textName, num);
  free(num);

  addNewText(textName, gachaDir, gachaFile);
}
```
### Fungsi createNewText merupakan kelanjutkan setelah file Json berhasil diparsing dan diambil nama dan rarity. Kemudian, fungsi createNewText akan membuat kalimat sesuai dengan perintah soal. Lalu, akan dituliskan ke file .txt yang sudah dibuat sebelumnya.

```
void addNewText(char textName[], int gachaDir, int gachaFile)
{
  FILE *fp;
  char *num;

  // create dirName
  char dirName[100];
  if (asprintf(&num, "%d", gachaDir) == -1)
  {
    perror("asprintf");
  }
  strcat(strcpy(dirName, "gacha_gacha/total_gacha_"), num);
  free(num);
  strcat(dirName, "/");
  strcat(dirName, "_gacha_");
  if (asprintf(&num, "%d", gachaFile) == -1)
  {
    perror("asprintf");
  }
  strcat(dirName, num);
  strcat(dirName, ".txt");
  free(num);
  fp = fopen(dirName, "ab");
  fputs(textName, fp);
  fputs("\n", fp);
  fclose(fp);
}

char *listWeapons[130];
char *listChars[48];

void getListJsonFile(char dir_name[])
{
  struct dirent *de;
  DIR *dr = opendir(dir_name);

  if (dir_name == "characters")
  {
    int i = 0;
    while ((de = readdir(dr)) != NULL)
    {
      if (strcmp(de->d_name, ".") != 0 && strcmp(de->d_name, "..") != 0)
      {
        listChars[i] = (char *)malloc(strlen(de->d_name) + 1);
        strncpy(listChars[i], de->d_name, strlen(de->d_name));
        i++;
      }
    }
    closedir(dr);
  }
  else if (dir_name == "weapons")
  {
    int i = 0;
    while ((de = readdir(dr)) != NULL)
    {
      if (strcmp(de->d_name, ".") != 0 && strcmp(de->d_name, "..") != 0)
      {
        listWeapons[i] = (char *)malloc(strlen(de->d_name) + 1);
        strncpy(listWeapons[i], de->d_name, strlen(de->d_name));
        i++;
      }
    }
    closedir(dr);
  }
}
```

### Fungsi getListJsonFile digunakan untuk mendapatkan list file yang terdapat di direktori characters atau weapons. Hasilnya akan dimasukkan ke dalam array listWeapons atau listChars. Array ini digunakan untuk menentukan file mana yang akan diparse di fungsi getNewText.

```
int main()
{
  // POIN A
  char *down_args[] = {"wget", "-q", "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "char.zip", NULL};
  char *down_args2[] = {"wget", "-q", "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "weapons.zip", NULL};
  char *unz_args[] = {"unzip", "-q", "char.zip", NULL};
  char *unz_args2[] = {"unzip", "-q", "weapons.zip", NULL};
  char *rm_args[] = {"rm", "char.zip", NULL};
  char *rm_args2[] = {"rm", "weapons.zip", NULL};
  char *mkdir_args[] = {"mkdir", "gacha_gacha", NULL};

  downloadFile(down_args);
  downloadFile(down_args2);
  unzipFile(unz_args);
  unzipFile(unz_args2);
  rmFile(rm_args);
  rmFile(rm_args2);
  mkdirFile(mkdir_args);

  // Create Random File
  createNewDir();
  createNewTxt();
  getListJsonFile("characters");
  getListJsonFile("weapons");
  getNewText();
}
```
#### Fungsi main berisi beberapa array args untuk mendownload, unzip, remove file dan membuat direktori baru. Kemudian, array - array tersebut akan dilempar ke beberapa fungsi yang telah dijelaskan sebagai argumen. Setelah itu, Direktori dan file txt akan dibuat untuk menampung hasil parsingan. ListJson akan diambil dan gacha akan mulai dilakukan.


## Soal Shift 2
Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat
ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea
yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah
mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review,
tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan
poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan
untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun
untuk menyelesaikan pekerjaannya.

- Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang
diberikan ke dalam folder “/home/[user]/shift2/drakor”. Karena atasan Japrun
teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka
program harus bisa membedakan file dan folder sehingga dapat memproses file yang
seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.
- Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus
membuat folder untuk setiap jenis drama korea yang ada dalam zip. Karena kamu
tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan
folder-folder yang dibutuhkan sesuai dengan isi zip.
Contoh: Jenis drama korea romance akan disimpan dalam “/drakor/romance”, jenis
drama korea action akan disimpan dalam “/drakor/action” , dan seterusnya.
- Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder
dengan kategori yang sesuai dan di rename dengan nama.
Contoh: “/drakor/romance/start-up.png”.
- Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di
pindah ke masing-masing kategori yang sesuai. Contoh: foto dengan nama
“start-up;2020;romance_the-k2;2016;action.png” dipindah ke folder
“/drakor/romance/start-up.png” dan “/drakor/action/the-k2.png”.
- Di setiap folder kategori drama korea buatlah sebuah file "data.txt" yang berisi nama
dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting
list serial di file ini berdasarkan tahun rilis (Ascending). Format harus sesuai contoh
dibawah ini.
```
kategori : romance
nama : start-up
rilis : tahun 2020
nama : twenty-one-twenty-five
rilis : tahun 2022
```

Note dan Ketentuan Soal:
- File zip berada dalam drive modul shift ini bernama drakor.zip
- File yang penting hanyalah berbentuk .png
- Setiap foto poster disimpan sebagai nama foto dengan format [nama]:[tahun
rilis]:[kategori]. Jika terdapat lebih dari satu drama dalam poster, dipisahkan
menggunakan underscore(_).
- Tidak boleh menggunakan fungsi system(), mkdir(), dan rename() yang tersedia di
bahasa C.
- Gunakan bahasa pemrograman C (Tidak boleh yang lain).
- Folder shift2, drakor, dan kategori dibuatkan oleh program (Tidak Manual).
- [user] menyesuaikan nama user linux di os anda.



## Soal Shift 3 (revisi)

Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang

### A. Membuat folder modul2
Untuk  membuat folder di bahasa c digunakanlah execlp untuk menjalankan argumen sebagai command lalu digunakan -p agar jika parent folder belom dibuat maka parent folder akan dibuat juga. Lalu digunakan "while(wait(&status)) > 0" agar tidak exit dari function sebelum tugas dalam function selesai dikerjakan.

![image.png](./image.png)

#### 1. Membuat Folder darat
Gunakan function makeDirectory dengan memasukkan path untuk folder darat pada main.

`char pathDarat[] = "/home/lax/modul2/darat/";`
`makeDirectory(pathDarat);`


#### 2. Setelah 3s Membuat Folder air
Gunakan function makeDirectory dengan memasukkan path untuk folder air pada main.

`char pathAir[] = "/home/lax/modul2/air/";`
`sleep(3);`
`makeDirectory(pathAir);`



### B. Melakukan Unzip File animal.zip
Untuk menjalankan command unzip di bahasa c digunakanlah execlp diisi dengan command yang ingin dijalankan yaitu unzip dan digunakan -d untuk membuat destinasi untuk hasil dari unzip dan digunakan -q ("quiet") agar tidak menghasilkan verbose pada terminal. Digunakkan while(wait(&status)) untuk dapat menghindari exit function sebelum script selesai dijalankan.

![image-8.png](./image-8.png)


Gunakan function unzip dengan mengisi path untuk target dan destination pada main.

`char path[] = "/home/lax/modul2/";`
`char targetZIP[] = "animal.zip";`
`unzip(path, targetZIP);`


### C. Memindahkan file dari animal.zip
Pertama buka folder yang diinginkan yaitu folder animal lalu melakukan iterasi selama tidak NULL atau selama cursor file masih ada. Lalu setiap iterasi file pada folder dilakukan pengcopyan nama file (dir->d_name) ke array global hewan serta menambahkan sebanyak satu pada variabel count untuk menghitung berapa banyak jumlah file yang ada pada folder yang dibuka. Jika sudah selesai melakukan iterasi pada folder, folder akan ditutup. Kemudian list hewan sesuai kategori yang ada yaitu "air", "darat", dan "burung" dengan melakukan iterasi sebanyak jumlah hewan yang ada. Jika nama hewan mengandung kata "burung" (dapat digunakan fungsi string strstr) maka nama hewan tersebut akan dicopy ke array burung dan variabel burungSize akan ditambah sebanyak 1 (banyaknya jumlah file yang mengandung kata burung). Lalu jika hewan juga mengandung kata "darat" maka nama hewan tersebut juga akan dicopy ke array hewanDarat dan variabel countDarat akan ditambah sebanyak 1 (banyaknya jumlah file mengandung kata darat) dan akan melakukan pemanggilan function moveAnimalDaratOrAir dengan parameter type adalah "darat". Namun jika nama hewan mengandung kata "air" maka nama hewan tersebut akan dicopy ke array hewanAir dan variabel countAir akan ditambah sebanyak 1 dan akan melakukan pemanggilan function moveAnimalDaratOrAir dengan parameter type adalah "air".

![image-7.png](./image-7.png)

Untuk memindahkan dapat digunakan dengan "execlp("mv", "mv", path, dest, NULL)" dimana path adalah path source dari file dan dest adalah path source setelah dipindahkan nantinya. Untuk menentukan pembagiannya sesuai kategori digunakanlah strcmp antara type dan "air" dimana jika menghasilkan 0 maka artinya type dan "air" sama dan merupakan kategori air maka melakukan pengcopyan path air ke variabel dest, begitupun sebaliknya. Digunakkan while(wait(&status)) untuk dapat menghindari exit function sebelum script selesai dijalankan.

![image-6.png](./image-6.png)


Dipanggillah function listAnimal dengan path animal untuk memindahkan file ke folder kategorinya masing-masing dan melakukan penglistan nama file hewan ke global array yang telah disediakan sesuai dengan kategori air dan darat pada fungsi main.

`char pathAnimal[] = "/home/lax/modul2/animal/";`
`listAnimal(pathAnimal);`



#### 1. Menghapus file yang tidak mengandung baik "air" maupun "darat"
Untuk dapat menghapus file serta folder yang tidak dibutuhkan yaitu animal dan file yang tidak sesuai dengan format dapat dilakukan langsung dengan rm -r ("recursive") path. Lalu digunakan while wait untuk dapat menghindari exit function sebelum script selesai dijalankan.

![image-5.png](./image-5.png)


Karena kita ingin menghapus file yang berada di dalam folder animal serta folder animal, maka kita bisa langsung saja menghapus satu direktori dengan menggunakan function removeDirectory dan memparse path untuk animal pada main.

`char pathAnimal[] = "/home/lax/modul2/animal/";`
`removeDirectory(pathAnimal);`



### D. Menghapus file yang mengandung "burung" dari folder darat
Untuk dapat menghapus file yang diinginkan dapat dilakukan langsung dengan rm --force path. Lalu digunakan while wait untuk dapat menghindari exit function sebelum script selesai dijalankan.

![image-4.png](./image-4.png)


Melakukan concanate string path darat dengan nama file burung lalu melakukan penginterasian sebanyak jumlah file yang mengandung kata "burung" pada file darat.

![image-3.png](./image-3.png)


Memanggil function removeBirdFile yang telah dijelaskan diatas pada main

`removeBirdFile();`

### E. Melakukan list untuk nama file dari folder air ke list.txt
Untuk format nama list yang digunakkan adalah UID_UID Permission(rwx)_namafile.jpg. Untuk mendapat UID dapat dengan mudah bisa didapatkan dengan melakukan check stat pada folder air lalu digunakanlah getpwuid dari path yang diisi, lalu untuk mendapat nama dapat dengan mudah dilakukan pw->pw_name. Lalu untuk mendapat UID Permission didapatkan dengan membuat char array dan dicek satu persatu untuk read, write, dan execute dengan S_IRUSR(untuk r), S_IRUSR(untuk w), dan S_IRUSR(untuk x). Lalu untuk tahap terakhir dapat digunakan dengan I/O pada C. Digunakkan open file dengan mode "a" (append) dan untuk menulis per line digunakan fprintf dengan format yang telah diminta. Lalu setelah selesai menggunakan file, akan dilakukan penutupan file untuk mencegah memory leak

![image-2.png](./image-2.png)


Panggil function AddList sebanyak file yang berada dalam folder air pada main.

![image-1.png](./image-1.png)

### Hasil

![image-9.png](./image-9.png)

![image-10.png](./image-10.png)
