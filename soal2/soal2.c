#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>

#define HOME "/home/deiyl"
#define ZIPFILE HOME "/drakor.zip"

struct Data
{
    char *judul;
    int tahun;
    char *kategori;
    char *nama_file;
};

struct Node
{
    struct Data *drakor;
    struct Node *next;
};

struct Node *head;

void create_dir(char *dirname)
{
    pid_t pid = fork();
    int status;
    if (pid == 0)
    {

        char *argv[] = {"mkdir", dirname, NULL};
        execv("/usr/bin/mkdir", argv);
        exit(0);
    }
    wait(&status);
}

void extract(char *zippath, char *extractpath)
{
    pid_t pid = fork();
    int status;
    if (pid == 0)
    {
        char *argv[] = {"unzip", "-q", zippath, "-d", extractpath, NULL};
        execv("/usr/bin/unzip", argv);
        exit(0);
    }
    wait(&status);
}

void delete_unused()
{
    struct dirent *dp;
    DIR *dir = opendir(HOME "/shift2/drakor");

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        char *d_name = malloc(strlen(dp->d_name) + 1);
        strcpy(d_name, dp->d_name);

        if (strcmp(d_name + strlen(d_name) - 4, ".png") != 0 && strcmp(d_name, ".") != 0 && strcmp(d_name, "..") != 0)
        {
            pid_t pid = fork();
            int status;
            char *to_delete = malloc(100);
            strcpy(to_delete, HOME "/shift2/drakor/");
            strcat(to_delete, d_name);
            if (pid == 0)
            {
                char *argv[] = {"rm", to_delete, "-rf", NULL};
                execv("/usr/bin/rm", argv);
                exit(0);
            }
            wait(&status);
        }
    }
    closedir(dir);
}

void fetch_data()
{
    struct dirent *dp;
    DIR *dir = opendir(HOME "/shift2/drakor");

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, ".."))
        {
          
            int length = strlen(dp->d_name);
            char *filename = malloc(length + 1);
            strcpy(filename, dp->d_name);
            filename[length - 4] = '\0';

            char *token;
            char *rest = filename;
           
            while ((token = strtok_r(rest, "_", &rest)))
            {
                char *token2;
                char *rest2 = token;
                token2 = strtok_r(rest2, ";", &rest2);

                struct Node *tmp = (struct Node *)malloc(sizeof(struct Node));
                tmp->drakor = (struct Data *)malloc(sizeof(struct Data));

                tmp->drakor->nama_file = malloc(strlen(dp->d_name) + 1);
                strcpy(tmp->drakor->nama_file, dp->d_name);

                tmp->drakor->judul = malloc(strlen(token2) + 1);
                strcpy(tmp->drakor->judul, token2);

            
                token2 = strtok_r(rest2, ";", &rest2);
                tmp->drakor->tahun = atoi(token2);

                token2 = strtok_r(rest2, ";", &rest2);
                tmp->drakor->kategori = malloc(strlen(token2) + 1);
                strcpy(tmp->drakor->kategori, token2);

                if (head == NULL || head->drakor->tahun >= tmp->drakor->tahun)
                {
                    tmp->next = head;
                    head = tmp;
                }
                else
                {
                    struct Node *current = head;
                    while (current->next != NULL && current->next->drakor->tahun < tmp->drakor->tahun)
                    {
                        current = current->next;
                    }
                    tmp->next = current->next;
                    current->next = tmp;
                }
            }
        }
    }
    closedir(dir);
}

void create_dirs_and_txt()
{
    struct Node *tmp = head;
    do
    {
        char *name = malloc(100);
        strcpy(name, HOME);
        strcat(name, "/shift2/drakor/");
        strcat(name, tmp->drakor->kategori);
        DIR *dir = opendir(name);
      
        if (dir)
        {
            closedir(dir);
        }
        else
        {
            create_dir(name);
        }
        FILE *fptr;
        strcat(name, "/data.txt");
        fptr = fopen(name, "w");
        if (fptr) {
            fprintf(fptr, "kategori : %s", tmp->drakor->kategori);
            fclose(fptr);
        }
    } while (tmp = tmp->next);
}

void move_files() {
    struct Node *tmp = head;
    do
    {
        pid_t pid = fork();
        int status;
        if (pid == 0)
        {
            char *src = malloc(100);
            strcpy(src, HOME);
            strcat(src, "/shift2/drakor/");
            strcat(src, tmp->drakor->nama_file);
            char *dst = malloc(100);
            strcpy(dst, HOME);
            strcat(dst, "/shift2/drakor/");
            strcat(dst, tmp->drakor->kategori);
            strcat(dst, "/");
            strcat(dst, tmp->drakor->judul);
            strcat(dst, ".png");
            char *argv[] = {"cp", src, dst, NULL};
            execv("/usr/bin/cp", argv);
            exit(0);
        }
        wait(&status);
    } while (tmp = tmp->next);
}

void list_dramas() {
    struct Node *tmp = head;
    do
    {
        char *filename = malloc(100);
        strcpy(filename, HOME);
        strcat(filename, "/shift2/drakor/");
        strcat(filename, tmp->drakor->kategori);
        strcat(filename, "/data.txt");

        FILE *fptr;
        fptr = fopen(filename, "a");
        if (fptr) {
            fprintf(fptr, "\n\nnama : %s\nrilis  : tahun %d", tmp->drakor->judul, tmp->drakor->tahun);
            fclose(fptr);
        }
    } while (tmp = tmp->next);
}

void cleanup() {
    struct Node *tmp = head;
    do
    {
        char *filename = malloc(100);
        strcpy(filename, HOME);
        strcat(filename, "/shift2/drakor/");
        strcat(filename, tmp->drakor->nama_file);
        pid_t pid = fork();
        int status;
        if (pid == 0)
        {
            char *argv[] = {"rm", filename, "-f", NULL};
            execv("/usr/bin/rm", argv);
            exit(0);
        }
        wait(&status);
    } while (tmp = tmp->next);
}

void main()
{
    create_dir(HOME "/shift2");
    create_dir(HOME "/shift2/drakor");
    extract(ZIPFILE, HOME "/shift2/drakor");
    delete_unused();
    fetch_data();
    create_dirs_and_txt();
    move_files();
    list_dramas();
    cleanup();
}
